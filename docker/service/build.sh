#!/bin/bash
mkdir /root/.ssh/
cp /service/keys/* /root/.ssh/
chmod -R 0600 /root/.ssh/
chmod -R 0777 /service/
git clone git@gitlab.com:vladimir.samuylov/tdg1.git /app/apps/tdg1
git clone git@gitlab.com:vladimir.samuylov/tdg2.git /app/apps/tdg2

#pull in first repository
cd /app/apps/tdg1
git pull
dos2unix projectBuilder.sh
sh projectBuilder.sh

#pull in second repository
cd /app/apps/tdg2
git pull
dos2unix projectBuilder.sh
sh projectBuilder.sh


chmod -R 0777 /app/apps/